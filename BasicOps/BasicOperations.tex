% Created 2017-12-07 Thu 19:47
% Intended LaTeX compiler: pdflatex
\documentclass[a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\author{R.S. Bhalla}
\date{\today}
\title{Basic Operations in R}
\hypersetup{
 pdfauthor={R.S. Bhalla},
 pdftitle={Basic Operations in R},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 25.3.2 (Org mode 9.1.4)}, 
 pdflang={English}}
\begin{document}

\maketitle
\setcounter{tocdepth}{2}
\tableofcontents


\section*{Summary}
\label{sec:orge430dd5}

For those beginning programming in R, it might seem a little intimidating. This tutorial is to walk you through some of the very basic operations in R to familiarise you with the syntax and behaviour of the software.

Please note, the commands provided here are generic R commands, i.e. they should work equally well on a simple console or a full fledged IDE such as RStudio.

You should be able to run these commands by:

\begin{enumerate}
\item By calling the corresponding script in a console \texttt{Rscript [R file]} where [R file] refers to the R script.
\item By calling the command \texttt{R CMD BATCH [R file]} which creates a file with the name as the script and with the suffix "Rout".
\item By first starting an instance of R in the console by typing \texttt{R} and then running the command: \texttt{source("[R file]")}. or finally
\item Start an instance of R and then copy paste each command into the console one by one.
\end{enumerate}

The chances are that you are using the RStudio or Emacs with the ESS interface for this workshop. There are other interfaces available, some which have a built in console where you can type commands in and get corresponding results and messages. 

\section*{Setting up the environment}
\label{sec:org80a6c6a}

\subsection*{Listing files and directories}
\label{sec:org0bfe5cc}

The first step when starting an instance of R is to ensure you've set the working directory correctly. This ensures that your script can read and write to a specified location. If not set, R takes the directory it is launched from as its working directory.

The script below does the following:

\begin{enumerate}
\item Sets a working directory.
\item Saves the name of the working directory to a vector called "wd".
\item Prints out the contents of the directory onto the screen.
\item Prints out all the files of the type ".csv" onto the screen.
\end{enumerate}


\begin{verbatim}
setwd("~/Events/Workshops/R_101/Tutorials/BasicOps")
wd <- getwd()
list.files(wd)
list.files(wd, pattern = "*.csv")
\end{verbatim}

\begin{verbatim}
BirdCount.csv
\end{verbatim}

Listing directories is equally straightforward. Here I list the various directories I have in my R tutorials folder. I use the symbol "../" to indicate I want to go one directory up from my present (working) directory.

\begin{verbatim}
list.dirs("../")
\end{verbatim}

\begin{center}
\begin{tabular}{ll}
../ & \\
..//BasicOps & \\
..//BasicOps/mypackage & \\
..//BasicOps/mypackage/R & \\
..//BasicOps/reshape2 & \\
..//BasicOps/reshape2/R & \\
..//BasicStats & \\
..//GLM & \\
..//MultipleLinearRegressions & \\
..//MultipleLinearRegressions/R Tutorial Series: Multiple Linear Regression & R-bloggers\_files\\
..//ObjectsInR & \\
..//ReshapeGGPlot & \\
\end{tabular}
\end{center}


Suppose I only wanted the names of the directories, without the sub-directories and without the leading dots and slashes, I'd simply add some switches to the command as thus:

\begin{verbatim}
list.dirs("../", full.names = FALSE, recursive = FALSE)
\end{verbatim}

\begin{center}
\begin{tabular}{l}
BasicOps\\
BasicStats\\
GLM\\
MultipleLinearRegressions\\
ObjectsInR\\
ReshapeGGPlot\\
\end{tabular}
\end{center}

\subsection*{Installing and loading additional packages}
\label{sec:org38eef48}

Once the basic file locations are set, you might want to load some packages into R to extend its functionality. For this exercise we'll load the following packages: "plyr", "ISwR", "reshape2", "ggplot2", "feather", "data.table", "plotly" and "animation".

The chances are that you've not installed some or all of these. 

\textbf{Note:} you need an active internet connection to do this.

Here is some code which you can run to do this quickly. The code does the following:

\begin{itemize}
\item Create a list of package names.
\item Install the list using the lapply (list apply) command.
\item Load the package and
\item Provide a short description of the packages.
\end{itemize}

\begin{verbatim}
pkglist <- c("animation", "plotly", "data.table", "feather", "ggplot2", "reshape2", "ISwR", "plyr")
install.packages(pkglist, dependencies = TRUE)
lapply(pkglist, library, character.only = TRUE)
lapply(pkglist, packageDescription, fields = "Title")
\end{verbatim}

This may take a while, depending on the speed of your internet connection. The command \texttt{install.pcakages} not only installs the concerned package but also other packages it might depend on. Please note that you may need to install additional (non-R) software libraries for some of the packages. You'll need to use the concerned operating system's package managing system for that.

Here is a quick summary of the packages:

\begin{center}
\begin{tabular}{ll}
Package & Description\\
animation & A Gallery of Animations in Statistics and Utilities to Create Animations\\
plotly & Create Interactive Web Graphics via 'plotly.js'\\
data.table & Extension of `data.frame`\\
feather & R Bindings to the Feather 'API'\\
ggplot2 & Create Elegant Data Visualisations Using the Grammar of Graphics\\
reshape2 & Flexibly Reshape Data: A Reboot of the Reshape Package\\
ISwR & Introductory Statistics with R\\
plyr & Tools for Splitting, Applying and Combining Data\\
\end{tabular}
\end{center}

And here are some of the base packages which are automatically loaded:

\begin{center}
\begin{tabular}{ll}
stats & The R Stats Package\\
graphics & The R Graphics Package\\
grDevices & The R Graphics Devices and Support for Colours and Fonts\\
utils & The R Utils Package\\
datasets & The R Datasets Package\\
methods & Formal Methods and Classes\\
base & The R Base Package\\
\end{tabular}
\end{center}

If you want to do this one package at a time the sequence of commands is:

\begin{itemize}
\item To install a package  \texttt{install.packages("PACKAGENAME", dependencies = TRUE)}
\item To load a package \texttt{library("PACKAGENAME")}
\item To get the description of the package \texttt{packageDescription("PACKAGENAME")}
\end{itemize}

\section*{Basic File Operations}
\label{sec:orgf32a4af}

\subsection*{Import data}
\label{sec:orgbf6f56c}

You can import various formats into R. Some IDE's such as RStudio provide you a nice GUI to do so as well. Here are a few examples.

Before beginning, remember that the files need to be "cleaned" to meet these conditions:

\begin{itemize}
\item No spaces in column names or file names. Use underscores or the period "." if you need.
\item No symbols in column names.
\item Comments not allowed in cells.
\item Replace all blank cells with "NA" (Not Available).
\item Add a blank line at the bottom of your file.
\end{itemize}

\textbf{Hint:} If you're working in a console but would prefer to have a dialogue box for searching for files you can use the command:
\texttt{file.choose()}

In Linux, I suggest you install the software "zenity" on your system and the package "svDialogs" in R and then use the command \texttt{dlgOpen()}. 

\begin{verbatim}
library(svDialogs)
\end{verbatim}

\subsubsection*{Importing from a delimited file}
\label{sec:org770e847}

This is the most common way of importing files into R. 

\begin{verbatim}
##  bc <- read.csv(dlgOpen(title = "Select one csv file", filters = dlgFilters[c("csv", "All"), ])$res)
bc <- read.csv(dlgOpen(title = "Select one csv file", filters = dlgFilters["All", ])$res)
summary(bc)
\end{verbatim}

\subsubsection*{Importing an excel file}
\label{sec:org128a5ed}

R can read both xls files and xlsx files using \href{http://readxl.tidyverse.org/}{the \texttt{readxl} package}. If you haven't installed it yet, do so now:

\texttt{install.packages("readxl", dependencies=TRUE)}

Now load the package:

Import the excel file, in this case we're loading the first sheet. Visit the package site for more options.

\begin{verbatim}
xlsx_example <- readxl_example("datasets.xlsx")
xlsx_sheet1 <- read_excel(xlsx_example, sheet = 1)
head(xlsx_sheet1)
\end{verbatim}

\begin{center}
\begin{tabular}{rrrrl}
5.1 & 3.5 & 1.4 & 0.2 & setosa\\
4.9 & 3 & 1.4 & 0.2 & setosa\\
4.7 & 3.2 & 1.3 & 0.2 & setosa\\
4.6 & 3.1 & 1.5 & 0.2 & setosa\\
5 & 3.6 & 1.4 & 0.2 & setosa\\
5.4 & 3.9 & 1.7 & 0.4 & setosa\\
\end{tabular}
\end{center}

\subsubsection*{Saving and loading an R binary}
\label{sec:orgff3076f}

The csv format is usually the most convenient way of sharing data frames in R. However, there are times when the data is not in a data frame (more on this on the section of kinds of data in R) or if the data frame is very large. In this case it is better to save and load the data as an R binary file which has the following advantages:

\begin{enumerate}
\item It can hold any type of data in R and import it without requiring any additional cleaning.
\item You can save a bunch of objects at one go.
\item If you use the \texttt{save.image} command, it will save the entire workspace (all the objects you're using).
\item You can specify different kinds of compression for the data, making it far easier to share large files.
\item The load command automatically restores the name of the object.
\end{enumerate}

To save an R binary you need to use the command:

\begin{verbatim}
save([NAME OF R OBJECTS TO BE SAVED], file = "Output *.rda")
unlink("Output *.rda")
\end{verbatim}

The unlink command removes the output file from your workspace to avoid confusion.

To load the binary use: 

\begin{verbatim}
load(file = [Output *.rda])
\end{verbatim}

Both \texttt{rda} and \texttt{RData} as used as file extensions for R objects.

The following switches add additional functionality to the "save" command:

\begin{itemize}
\item \texttt{list} You can create a list of character vectors of the objects to be saved in stead of writing down their names one at a time. For example if I have a data frame called \texttt{df1} and a function called \texttt{fn1} I can save them both to a R binary called df.fn thus:

\begin{verbatim}
save(df1, fn1, file = "~/df.fn")
\end{verbatim}

Or I could first create a list and then save them thus:

\begin{verbatim}
df.fn.lst <-list(df1, fn1)
save(list = df.fn.lst, file = "~/df.fn")
\end{verbatim}

\item \texttt{compress} This allows you to specify the type of compression that should be used. The options are "TRUE/FALSE", "gzip", "bzip2" or "xz". For example:

\begin{verbatim}
df.fn.lst <-list(df1, fn1)
save(list = df.fn.lst, file = "~/df.fn", compress = "bzip2")
\end{verbatim}
\end{itemize}

\textbf{Caution:} If you've got an object with the same name as the R binary, it will be overwritten when you load the binary. In such a situation, you'd bet better off using the \texttt{saveRDS} command. However this command can only save a single object and the \texttt{readRDS} command needs to be assigned to a new object, or it too will overwrite the existing object.

To load any R binary you need to type in the statement 

\begin{verbatim}
load([NAME OF RData FILE]
\end{verbatim}

\section*{\href{https://www.r-project.org/help.html}{Getting to know the R help system}}
\label{sec:org3115bad}

Lets now learn how to figure out what each command does. R has extensive documentation, not just on the internet, but also built into the packages themselves. All packages come with their own documentation, some better than others. Some even come with extensive usage examples. The basic commands you need to learn are as follows:

\begin{itemize}
\item If you place a question mark before a function as thus:  \texttt{?[function]} or if you ask for help thus: \textasciitilde{}help([function]), you'll open up its manual page.
\item If you want help about a package and not a function type: \texttt{help(package="[package]")}
\item If you want to see a usage example of a command type: \texttt{example([function])}.
\item Some packages come with extensive usage examples called "vignettes". To see the vignettes that are installed in your system already simply type: \texttt{browseVignettes()}.
\item If you want to read a vignette related to a specific package type: \texttt{browseVignettes(package="[package]")}.
\item To browse a specific vignette use: \texttt{vignette("vignette-name")} or \texttt{vignette("[function]", package="[package]")}, in case there are two functions with the same name in different packages.
\item The same logic applies for demonstrations of packages which are sometime bundled in to give the users a feel of how they work on their own machines. This is done with the command: \texttt{demo("[function]")} or \texttt{demo("[function]", package="[package]")}.
\item If you don't know the specific names of the function or package you can still search the built in help using a string (or a regular expression) that occurs in the package. You can do this by: \texttt{help.search()} and \texttt{??}.
\end{itemize}

Other than this R allows you to search the internet directly from the command: \texttt{RSiteSearch()} and the online documentation for R can be accessed using \texttt{help.start()}.

Finally, a resource that I use all the time is the \href{http://stackoverflow.com/}{StackOverflow} site which is highly recommended.
\end{document}
