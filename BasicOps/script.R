
setwd("~/Events/Workshops/R_101/Tutorials/BasicOps")
wd <- getwd()
list.files(wd)
list.files(wd, pattern = "*.csv")

library(plyr)
library(pander)
library(ISwR)
lapply(c("plyr", "pander", "ISwR"), library, character.only = TRUE)


bc <- read.csv("BirdCount.csv")                                                                 
head(bc)                                                                                        
or.sp <- order(bc$Species)                                                                      
bc <- bc[or.sp,]                                                                                
head(bc)                                                                                        
## library(plyr)                                                                                
## bc.consol <- ddply(bc,.(Site, Species),                                                      
               ##    summarise,Count = sum(Sightings))                                          
bc.consol <- aggregate(x=bc$Sightings,                                                          
                       by=bc[,c("Site","Species")], FUN=sum)                                    
colnames(bc.consol)[3] <- "Sightings"                                                           
head(bc.consol, n=15)                                                                           
                                                                                                
##-- missing values                                                                             
tmp1 <- read.csv(file="./data_ex1/null_rndcsv10.csv")                                           
tmp1[!is.na(tmp1$Num),]                                                                         
tmp1[!is.na(tmp1$Num) & !is.na(tmp1$Alph),]                                                     
complete.cases(tmp1)                                                                            
na.omit(tmp1)                                                                                   
a <- read.csv("./data_ex1/rndcsv15.csv")                                                        
b <- read.csv("./data_ex1/rndcsv20.csv")                                                        
rowbind <- rbind(a,b)                                                                           
a <- read.csv("./data_ex1/rndcsv15.csv")                                                        
b <- read.csv("./data_ex1/null_rndcsv15.csv")                                                   
colbind <- cbind(a,b)                                                                           
                                 ## ------------------------------------------------------------------------                     
                                                                                                
## ----R_object_ex1, echo=TRUE---------------------------------------------                     
## Try it out:                                                                                  
x <- 2 + 4                                                                                      
x * 2                                                                                           
                                                                                                
## ------------------------------------------------------------------------                     
                                                                                                
## ----R_Object_ex2, echo=TRUE, size='footnotesize', highlight=FALSE-------                     
typeof(x) ## storage mode of the object                                                         
class(x) ## names of the classes from which the object inherits                                 
str(x) ## structure of the object                                                               
summary(x) ## summary of the object                                                             
names(x) ## names of the object                                                                 
dim(x) ## dimensions of the object                                                              
length(x) ## how many non-missing values are there in the object                                
nrow(x)                                                                                         
ncol(x)                                                                                         
## fix(x) ## open x in a spreadsheet for editing                                                
                                                                                                
## ------------------------------------------------------------------------                     
                                                                                                
## ----logical_vectors, echo=TRUE------------------------------------------                     
x <- c("Blue", "Green", "Yellow", "Red", "Orange")                                              
y <- c("Blue", "Orange", "Yellow", "Red", "Green")                                              
x==y                                                                                            
                                                                                                
## ------------------------------------------------------------------------                     
                                                                                                
## ----create_matrix, echo=TRUE--------------------------------------------                     
newmat <- matrix(1:36, nrow=6, ncol=6)                                                          
colnames(newmat) <- LETTERS[1:6] ## same as c("A", "B", "C", "D")                               
rownames(newmat) <- month.abb[1:6] ## same as first 6 months                                    
newmat                                                                                          
                                                                                                
## ------------------------------------------------------------------------                     
                                                                                                
## ----create_factor, echo=TRUE--------------------------------------------                     
 quality <- c(1, 3, 2, 2, 5, 4)                                                                  
fquality <- factor(quality, levels=1:5)                                                         
levels(fquality) <- c("v-poor", "poor", "ok", "good", "v-good")                                 
## see the results                                                                              
fquality                                                                                        
as.numeric(fquality)                                                                            
                                                                                                
## ------------------------------------------------------------------------                     
                                                                                                
## ----create_list, size='footnotesize', echo=TRUE-------------------------                     
lst1 <- list(colors=x, months=mat, facts=fquality)                                              
lst1                                                                                            
                                                                                                
## ------------------------------------------------------------------------                     
                                                                                                
## ----create_dataframe, echo=TRUE-----------------------------------------                     
well.id <- c(letters[1:6])                                                                      
depth.m <- c(130, 220, 351, 592, 124, 112)                                                      
EC.s.m <- c(0.0245, 0.0142, 0.0183, 0.0449, 0.0582, 0.0335)                                     
ph <- c(7.3, 6.7, 8.4, 5.3, 7.9, 4.8)                                                           
df <- data.frame(well.id, depth.m, EC.s.m, ph)                                                  
df                                                                                              
                                                                                                
## ------------------------------------------------------------------------                     
                                                                                                
## ----indexing_vectors, size='tiny', echo=TRUE----------------------------                     
x[c(1,3,4)] ## select element 1, 3, and 4 from x                                                
x[c(1, 3)]<-c("Black", "Pink") ## replace element 1 and 3 with Black and Pink.                  
x[x=="Black"] ## logical question                                                               
x[x=="Black"]<-"White" ## conditional replace                                                   
x                                                                                               
x[x!="Pink"]                                                                                    
x[-c(1, 3, 4)] ## remove 1, 3 and 4th object from x                                             
                                                                                                
## ------------------------------------------------------------------------    ## ----indexing_lists1, size='tiny', echo=TRUE-----------------------------                     
names(lst1)                                                                                     
lst1$months                                                                                     
lst1$months[c(1, 3, 6)]                                                                         
rownames(lst1$months)[c(1, 3, 6)]                                                               
                                                                                                
## ------------------------------------------------------------------------                     
                                                                                                
## ----indexing_lists2, size='tiny', echo=TRUE-----------------------------                     
lst1[1]                                                                                         
class(lst1[1]) ## this is a list                                                                
lst1[[1]] ## this is the element of the list (colors)                                           
class(lst1[[1]])                                                                                
lst1[[1]][1] ## selects the first element of the vector colors which is in the list             
class(lst1[[1]][1])                                                                             
lst1[[1]][1] ## selects the first element of the vector colors which is in the list             
                                                                                                
## ------------------------------------------------------------------------                     
                                                                                                
## ----indexing_lists3, size='tiny', echo=TRUE-----------------------------                     
## now explain this                                                                             
lst1[[2]]                                                                                       
lst1[[2]][2]                                                                                    
lst1[[2]][2,]                                                                                   
lst1[[2]][2,3]                                                                                  
                                                                                                
## ------------------------------------------------------------------------                     
                                                                                                
## ----indexing_dataframes1, size='tiny', echo=TRUE------------------------                     
df[1,2] ## element in row 1 and col 2 of df - just like a matrix                                
df[2:4, 1:3] # return row 2-4 and col 1-3                                                       
df[-c(1,3,6),] ## removes rows 1, 3 and 6                                                       
df[,-c(1,3),] ## removes columns 1 and 3                                                        
                                                                                                
## ------------------------------------------------------------------------   ## ----indexing_dataframes2, size='tiny', echo=TRUE------------------------                     
df>4 ## conditional statement - warning given as df[,1] is factor                               
class(df[,1])                                                                                   
df[,4]>6.5 & df[,4]<7.5                                                                         
df$potable <- df[,4]>6.5 & df[,4]<7.5                                                           
df                                                                                              
                                                                                                
## ------------------------------------------------------------------------                     
                                                                                                
## ----indexing_dataframes3, size='tiny', echo=TRUE------------------------                     
## selecting part of a dataframe using a condition                                              
df$depth.m[df[,4]>6.5 & df[,4]<7.5]                                                             
## which is the same as                                                                         
df$depth.m[df$potable==TRUE]

## ------------------------------------------------------------------------                     
                                                                                                
## ----import_csv, echo=TRUE-----------------------------------------------                     
bc <- read.csv("./BirdCount.csv", header=TRUE)                                                  
head(bc)                                                                                        
                                                                                                
## ------------------------------------------------------------------------                     
                                                                                                
## ----sort_and_order1, size='tiny', echo=TRUE-----------------------------                     
or.sp <- order(bc$Species)                                                                      
bc <- bc[or.sp,] ## note how R allows you to overwrite an object by an operation on itself.     
head(bc, n=15)                                                                                  
                                                                                                
## ------------------------------------------------------------------------                     
                                                                                                
## ----plyr_to_consolidate, size='tiny', echo=TRUE-------------------------                     
library(plyr)                                                                                   
bc.consol <- ddply(bc,.(Site, Species), summarise,Count = sum(Sightings))                       
head(bc.consol, n=15)                                                                           
                                                                                                
## ------------------------------------------------------------------------                     
                                                                                                
## ----aggregate_to_consolidate, size='tiny', echo=TRUE--------------------                     
bc.consol <- aggregate(x=bc$Sightings, by=bc[,c("Site","Species")], FUN=sum)                    
colnames(bc.consol)[3] <- "Count"                                                               
head(bc.consol, n=15)                                                                           
                                                                                                
## ------------------------------------------------------------------------                     
                                                                                                
## ----import_file_to_fix_NAs, size='tiny', echo=TRUE----------------------                     
a <- read.csv("./rndcsv15.csv")                                                                 
head(a)                                                                                         
                                                                                                
## ------------------------------------------------------------------------                     
                                                                                                
## ----fix_NAs, size='tiny', echo=TRUE-------------------------------------     head(is.na(a))                                                                                  
complete.cases(a)                                                                               
head(na.omit(a))                                                                                
                                                                                                
## ------------------------------------------------------------------------                     
                                                                                                
## ----merge_eg, size='footnotesize', echo=TRUE----------------------------                     
a <- read.csv("rndcsv15.csv")                                                                   
b <- read.csv("null_rndcsv15.csv")                                                              
a.b <-  merge(a, b, by="X", all=TRUE)                                                           
head(a.b)

## ------------------------------------------------------------------------                     
                                                                                                
## ----plotting_histogram_from_factor_freq, size='footnotesize',fig.width=4,fig.height=4,out.wid
th='.45\\linewidth',fig.align='center', echo=TRUE----                                           
quality <- c(1, 3, 2, 2, 5, 4)                                                                  
fquality <- factor(quality, levels=1:5)                                                         
levels(fquality) <- c("v-poor", "poor", "ok", "good", "v-good")                                 
hist(table(fquality), xlab = "Frequency of Goodness", main = "")                                
                                                                                                
## ------------------------------------------------------------------------                     
                                                                                                
## ----gen__factor_freq_table, size='footnotesize', echo=TRUE--------------                     
library(pander)                                                                                 
panderOptions('knitr.auto.asis', FALSE)                                                         
pander(table(fquality))                                                                         
                                                                                                
## ------------------------------------------------------------------------                     
                                                                                                
## ----plot_eg_points, size='footnotesize',fig.width=4,fig.height=4,out.width='.45\\linewidth',f
ig.align='center', echo=TRUE----                                                                
x <- runif(50, 0, 2) ## Generate random points for x                                            
y <- runif(50, 0, 2) ## Generate random points for y                                            
plot(x,y)                                                                                       
                                                                                                
## ------------------------------------------------------------------------                     
                                                                                                
## ----plot_eg_lines, size='footnotesize',fig.width=4,fig.height=4,out.width='.45\\linewidth',fi
g.align='center', echo=TRUE----                                                                 
plot(x,y, type="l")                                                                             
                                                                                                
## ------------------------------------------------------------------------                     
  ## ------------------------------------------------------------------------

## ----plotting_histogram_from_factor_freq, size='footnotesize',fig.width=4,fig.height=4,out.width='.45\\linewidth',fig.align='center', echo=TRUE----
quality <- c(1, 3, 2, 2, 5, 4)
fquality <- factor(quality, levels=1:5)
levels(fquality) <- c("v-poor", "poor", "ok", "good", "v-good")
hist(table(fquality), xlab = "Frequency of Goodness", main = "")

## ------------------------------------------------------------------------

## ----gen__factor_freq_table, size='footnotesize', echo=TRUE--------------
library(pander)
panderOptions('knitr.auto.asis', FALSE)
pander(table(fquality))

## ------------------------------------------------------------------------

## ----plot_eg_points, size='footnotesize',fig.width=4,fig.height=4,out.width='.45\\linewidth',fig.align='center', echo=TRUE----
x <- runif(50, 0, 2) ## Generate random points for x
y <- runif(50, 0, 2) ## Generate random points for y
plot(x,y)

## ------------------------------------------------------------------------

## ----plot_eg_lines, size='footnotesize',fig.width=4,fig.height=4,out.width='.45\\linewidth',fig.align='center', echo=TRUE----
plot(x,y, type="l")

## ------------------------------------------------------------------------

## ----plot_eg_points_over_line, size='footnotesize',fig.width=4,fig.height=4,out.width='.45\\linewidth',fig.align='center', echo=TRUE----
x <- seq(0, 2*pi, len = 51)
## something "between type='b' and type='o'":
plot(x, sin(x), type = "o", main = 'Overplot')

## ------------------------------------------------------------------------

## ----plot_eg_points_line_joined, size='footnotesize',fig.width=4,fig.height=4,out.width='.45\\linewidth',fig.align='center', echo=TRUE----
plot(x, sin(x), type = "b", main = 'Type = "b", Empty plot')

## ------------------------------------------------------------------------

## ----plot_eg_points_types, size='tiny',fig.width=8,fig.height=4,out.width='.85\\linewidth',fig.align='center', echo=TRUE----
x <- c(1:5); y <- x # create some data
par(pch=22, col="blue") # plotting symbol and color
par(mfrow=c(2,4)) # all plots on one page
opts <- c("p","l","o","b","c","s","S","h")
for(i in 1:length(opts)){
  heading = paste("type=",opts[i])
  plot(x, y, main=heading)
  lines(x, y, type=opts[i])
}
par(mfrow=c(1,1))
## ------------------------------------------------------------------------

## ----plot_eg_hist, size='footnotesize',fig.width=4,fig.height=4,out.width='.45\\linewidth',fig.align='center', echo=TRUE----
hist(mtcars$mpg, breaks=24, col="green") 

## ------------------------------------------------------------------------

## ----plot_eg_combined1, size='footnotesize',fig.width=4,fig.height=4,out.width='.45\\linewidth',fig.align='center', echo=TRUE----
plot(x, sin(x), type = "o", main = 'Overplot')

## ------------------------------------------------------------------------

## ----plot_eg_combined2, size='footnotesize',fig.width=4,fig.height=4,out.width='.45\\linewidth',fig.align='center', echo=TRUE----
x<-mtcars$mpg 
h<-hist(x, breaks=10, col="red", xlab="Miles Per Gallon",
   main="Histogram with Normal Curve")
xfit<-seq(min(x),max(x),length=40)
yfit<-dnorm(xfit,mean=mean(x),sd=sd(x))
yfit <- yfit*diff(h$mids[1:2])*length(x)
lines(xfit, yfit, col="blue", lwd=2) 

## ------------------------------------------------------------------------

## ----plot_eg_combined3, size='tiny',fig.width=4,fig.height=4,out.width='.45\\linewidth',fig.align='center', echo=TRUE----
attach(mtcars)
plot(wt, mpg, main="Scatterplot Example",
   xlab="Car Weight ", ylab="Miles Per Gallon ", pch=19) ## scatter plot
abline(lm(mpg~wt), col="red") # regression line (y~x)
lines(lowess(wt,mpg), col="blue") # lowess line (x,y)  ## Add fit lines

## ------------------------------------------------------------------------

## ----plot_box, size='tiny',fig.width=4,fig.height=4,out.width='.45\\linewidth',fig.align='center', echo=TRUE----
boxplot(wt)
detach(mtcars)

## ------------------------------------------------------------------------

## ----ecdf_plot, size='tiny',fig.width=4,fig.height=4,out.width='.45\\linewidth',fig.align='center', echo=TRUE----
x <- runif(100, 0, 2) ## Generate random points for x
n <- length(x)
plot(sort(x), (1:n)/n, type="s", ylim=c(0,1)) ## note the type of plot is "s"

## ------------------------------------------------------------------------

## ----qqplot, size='tiny',fig.width=4,fig.height=4,out.width='.45\\linewidth',fig.align='center', echo=TRUE----
qqnorm(x)

## ------------------------------------------------------------------------

## ----plot_box3, size='tiny',fig.width=4,fig.height=3,out.width='.45\\linewidth',fig.align='center', echo=TRUE----
library(ISwR)
attach(energy)
summary(energy)
boxplot(expend~stature, notch=TRUE)


x <- c(1:5)
y <-c(1:5)


## ------------------------------------------------------------------------

## ----ttest_onesample, size="footnotesize", echo=TRUE---------------------
daily.intake <- c(5260,5470,5640,6180,6390,6515, 6805,7515,7515,8230,8770)
summary(daily.intake)
t.test(daily.intake,mu=7725)

## ------------------------------------------------------------------------

## ----wilcox_onesample, size="footnotesize", echo=TRUE--------------------
wilcox.test(daily.intake, mu=7725)

## ------------------------------------------------------------------------

## ----ttest_twosample, size="footnotesize", echo=TRUE---------------------
library(ISwR)
data(energy)
attach(energy)
head(energy)
t.test(expend~stature, var.equal=T) ##expend is described by stature

## ------------------------------------------------------------------------

## ----wilcoxtest_twosample, size="footnotesize", echo=TRUE----------------
wilcox.test(expend~stature)
detach(energy)

## ------------------------------------------------------------------------

## ----ttest_paired, size="footnotesize", echo=TRUE------------------------
data(intake)
attach(intake)
head(intake)
t.test(pre, post, paired=T)

## ------------------------------------------------------------------------

## ----wilcoxtest_matchedpairs, size="footnotesize", echo=TRUE-------------
wilcox.test(pre, post, paired=T)

## ------------------------------------------------------------------------

## ----regression_linear, size="footnotesize", echo=TRUE-------------------
data(thuesen)
attach(thuesen)
lm.res <- lm(short.velocity~blood.glucose)
summary(lm.res)

## ------------------------------------------------------------------------

## ----regression_lmplot, size="footnotesize",fig.width=4,fig.height=4,out.width='.45\\linewidth',fig.align='center', echo=TRUE----
plot(blood.glucose,short.velocity)
abline(lm(short.velocity~blood.glucose))

## ------------------------------------------------------------------------

## ----corr_pearson, size="footnotesize", echo=TRUE------------------------
cor.test(blood.glucose,short.velocity,use="complete.obs")

## ------------------------------------------------------------------------

## ----corr_spearmans, size="footnotesize", echo=TRUE----------------------
cor.test(blood.glucose,short.velocity,method="spearman")

## ------------------------------------------------------------------------

## ----corr_kendal, size="footnotesize", echo=TRUE-------------------------
cor.test(blood.glucose,short.velocity,method="kendall")

## ------------------------------------------------------------------------

## ----anova, size="footnotesize", echo=TRUE-------------------------------
data(red.cell.folate)
attach(red.cell.folate)
anova(lm(folate~ventilation))

## ------------------------------------------------------------------------

## ----anova_plot, size="footnotesize",fig.width=4,fig.height=4,out.width='.45\\linewidth',fig.align='center', echo=TRUE----
xbar <- tapply(folate, ventilation, mean)
s <- tapply(folate, ventilation, sd)
n <- tapply(folate, ventilation, length)
sem <- s/sqrt(n)
stripchart(folate~ventilation, pch=16,vert=T)
arrows(1:3,xbar+sem,1:3,xbar-sem,angle=90,code=3,length=.1)
lines(1:3,xbar,pch=4,type="b",cex=2)

## ------------------------------------------------------------------------

## ----KW, size="footnotesize", echo=TRUE----------------------------------
kruskal.test(folate~ventilation)

