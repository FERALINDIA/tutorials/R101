% Created 2017-12-07 Thu 19:41
% Intended LaTeX compiler: pdflatex
\documentclass[a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\author{R.S. Bhalla}
\date{\today}
\title{Objects in R}
\hypersetup{
 pdfauthor={R.S. Bhalla},
 pdftitle={Objects in R},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 25.3.2 (Org mode 9.1.4)}, 
 pdflang={English}}
\begin{document}

\maketitle
\setcounter{tocdepth}{2}
\tableofcontents


\section*{Summary}
\label{sec:org3aac75b}

This tutorial covers the major types of objects which are used to store data in R and their manipulation. The focus will be on data frames. Please read the \href{https://cran.r-project.org/doc/manuals/r-release/R-lang.html}{official documentation} for a summary of the topic.

\section*{Introduction}
\label{sec:org8f639f8}

Objects in R are specialised data structures and provide a means to access data stored in memory. Thus, everything you access, manipulate or create in R is an object. Objects can belong to any of six atomic (or single) classes namely logical, integer, real, complex, string (or character) and raw. We won't be dealing with the raw class for most of our operations.

We are concerned with five major objects or classes of data structures in R. These are:

\begin{itemize}
\item atomic vectors: The simplest R data type with a single primitive or base type. You can find out the type using the commands below:
\begin{verbatim}
vec <- 1:30
print(vec)
typeof(vec) # type of the R object from the perspective of R
str(vec) # structure of the object
class(vec) # type of the object from the point of view of object oriented programming in R
\end{verbatim}

\begin{verbatim}
 [1]  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25
[26] 26 27 28 29 30
[1] "integer"
 int [1:30] 1 2 3 4 5 6 7 8 9 10 ...
[1] "integer"
\end{verbatim}

\item factors: categorical variables which can either be stored as purely nominal or in ordered levels. In the latter case an integer array is used to specify the actual levels and a second array of names is mapped to the integers. Read \href{https://www.stat.berkeley.edu/classes/s133/factors.html}{this post} for a more detailed treatment.
\begin{verbatim}
fct1 <- sample(letters[1:10], size = 30, replace = TRUE)
class(fct1) # the class is character
fct1 <-factor(fct1, ordered = TRUE) # ordering makes it possoble for comparisons
class(fct1) # now the class is an ordered factor
fct2 <- sample(letters[1:10], size = 30, replace = TRUE)
fct2 <-factor(fct2, ordered = TRUE)
table(fct1)  # summary of the levels and of often they occur
levels(fct1) # what are the levels
print(fct1)
print(c(fct1)) # the concatenate command converts each factor into its associated integer.
fct1 > fct2 # since they are ordere is possible to see which element in fct1 is greater than the corresponding elecment in fct2.
\end{verbatim}

\begin{verbatim}
[1] "character"
[1] "ordered" "factor"
fct1
b c d e f g h i j 
2 5 6 3 5 5 1 2 1
[1] "b" "c" "d" "e" "f" "g" "h" "i" "j"
 [1] d e f j f f g i c c d d d g e d g f b c g d e i h c g c b f
Levels: b < c < d < e < f < g < h < i < j
 [1] 3 4 5 9 5 5 6 8 2 2 3 3 3 6 4 3 6 5 1 2 6 3 4 8 7 2 6 2 1 5
Error in Ops.ordered(fct1, fct2) : level sets of factors are different
\end{verbatim}

In some cases, this code will throw an error - explain why.

\item matrices: a collection of \textbf{the same type of data} arranged in a two dimensional layout, i.e. nrows X ncols.
\begin{verbatim}
mat <- matrix(data = c(1:10, letters[1:10]), nrow =10, dimnames = list(paste("row:",1:10), c("nums", "alph")))
print(mat)
typeof(mat) # type of the R object from the perspective of R
str(mat) # structure of the object
class(mat) # type of the object from the point of view of object oriented programming in R
dim(mat) # dimensions of the matrix
\end{verbatim}

\begin{verbatim}
	nums alph
row: 1  "1"  "a" 
row: 2  "2"  "b" 
row: 3  "3"  "c" 
row: 4  "4"  "d" 
row: 5  "5"  "e" 
row: 6  "6"  "f" 
row: 7  "7"  "g" 
row: 8  "8"  "h" 
row: 9  "9"  "i" 
row: 10 "10" "j"
[1] "character"
 chr [1:10, 1:2] "1" "2" "3" "4" "5" "6" "7" "8" "9" "10" "a" "b" "c" "d" ...
 - attr(*, "dimnames")=List of 2
  ..$ : chr [1:10] "row: 1" "row: 2" "row: 3" "row: 4" ...
  ..$ : chr [1:2] "nums" "alph"
[1] "matrix"
[1] 10  2
\end{verbatim}

You'd have noticed that even though the column "nums" was integers, it was classed at character.

Try and generating a 3x8 matrix of numbers with appropriate headings.

\item data frames: a collection of \textbf{different types of data} arranged in a two dimensional layout, something like a spreadsheet or a csv file. You will be using this most often. 
\begin{verbatim}
datfrm <- data.frame(nums = c(1:30), ALPH = sample(LETTERS[1:10], 30, replace = TRUE), fct1, stringsAsFactors = FALSE)
head(datfrm)
typeof(datfrm) # type of the R object from the perspective of R
str(datfrm) # structure of the object
class(datfrm) # type of the object from the point of view of object oriented programming in R
dim(datfrm) # dimensions of the dataframe
\end{verbatim}

\begin{verbatim}
  nums ALPH fct1
1    1    G    d
2    2    I    e
3    3    J    f
4    4    D    j
5    5    E    f
6    6    F    f
[1] "list"
'data.frame':	30 obs. of  3 variables:
 $ nums: int  1 2 3 4 5 6 7 8 9 10 ...
 $ ALPH: chr  "G" "I" "J" "D" ...
 $ fct1: Ord.factor w/ 9 levels "b"<"c"<"d"<"e"<..: 3 4 5 9 5 5 6 8 2 2 ...
[1] "data.frame"
[1] 30  3
\end{verbatim}
\end{itemize}



\begin{itemize}
\item lists: a generic vector containing other types of objects. You can think of a list as a container which can hold any other type of object, including more lists. 

\begin{verbatim}
lst <- list(datfrm, mat, fct1, vec)
typeof(lst)
class(lst)
str(lst)
dim(lst)
\end{verbatim}

\begin{verbatim}
[1] "list"
[1] "list"
List of 4
 $ :'data.frame':	30 obs. of  3 variables:
  ..$ nums: int [1:30] 1 2 3 4 5 6 7 8 9 10 ...
  ..$ ALPH: chr [1:30] "G" "I" "J" "D" ...
  ..$ fct1: Ord.factor w/ 9 levels "b"<"c"<"d"<"e"<..: 3 4 5 9 5 5 6 8 2 2 ...
 $ : chr [1:10, 1:2] "1" "2" "3" "4" ...
  ..- attr(*, "dimnames")=List of 2
  .. ..$ : chr [1:10] "row: 1" "row: 2" "row: 3" "row: 4" ...
  .. ..$ : chr [1:2] "nums" "alph"
 $ : Ord.factor w/ 9 levels "b"<"c"<"d"<"e"<..: 3 4 5 9 5 5 6 8 2 2 ...
 $ : int [1:30] 1 2 3 4 5 6 7 8 9 10 ...
NULL
\end{verbatim}
\end{itemize}

\section*{Indexing Objects}
\label{sec:org0507d04}

Now that we've got a basic handle on the major types of data structures in R, lets go through some exercises on how to manipulate them.

Each class of objects has a different set of methods associated with how it is manipulated. Indexing is the term used to describe how one gets access to individual elements or subsets of a given object. The basic construction of an index is:

\begin{verbatim}
x[i]
x[i, j]
x[[i]]
x[[i, j]]
x$a
x$"a"
\end{verbatim}

Now lets try and apply this to the basic object types.

\begin{verbatim}
vec[1]
vec[c(1, 5, 10)]
try(vec[1, 1])
\end{verbatim}

\begin{verbatim}
[1] 1
[1]  1  5 10
Error in vec[1, 1] : incorrect number of dimensions
\end{verbatim}


Here we extracted the first, fifth and tenth element of the vector, but when we tried the form \texttt{x[i, j]} we threw and error. Why? 

Lets try this on a the matrix:

\begin{verbatim}
mat[1]
mat[c(1, 5, 10, 15, 25)] # list the first, fifth, tenth, fifteenth and twenty fifty element.
mat[, c(1, 2)] # list all rows and column 1 and 2e
mat[1, 2] # list the element in the first row and second column
mat[5, ] # list the element in the fifth row and all columns
\end{verbatim}

\begin{verbatim}
[1] "1"
[1] "1"  "5"  "10" "e"  NA
        nums alph
row: 1  "1"  "a" 
row: 2  "2"  "b" 
row: 3  "3"  "c" 
row: 4  "4"  "d" 
row: 5  "5"  "e" 
row: 6  "6"  "f" 
row: 7  "7"  "g" 
row: 8  "8"  "h" 
row: 9  "9"  "i" 
row: 10 "10" "j"
[1] "a"
nums alph 
 "5"  "e"
\end{verbatim}

\subsection*{Using column and row names while using indices on data frames}
\label{sec:orga752557}

We can repeat all the indexing operations we ran on the matrix on the data frame. However there are some additional operations you can use with the dollar sign "\$" and the column names as well. We use the \texttt{head()} function to only display the first 6 results.

\begin{verbatim}
names(datfrm)
datfrm$nums
datfrm$"nums"
head(datfrm["nums"])
head(datfrm["fct1"])
\end{verbatim}

\begin{verbatim}
[1] "nums" "ALPH" "fct1"
 [1]  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25
[26] 26 27 28 29 30
 [1]  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25
[26] 26 27 28 29 30
  nums
1    1
2    2
3    3
4    4
5    5
6    6
  fct1
1    d
2    e
3    f
4    j
5    f
6    f
\end{verbatim}

As you can see, the results are different classes. The "\$" sign simply outputs a vector while the indexing command outputs a dataframe. As in the case of matrices, you can select elements of the dataframe if you provide a row and column number or name.

\subsection*{Indices on lists}
\label{sec:org7688ba5}

Indices on lists work slightly differently from dataframes. Here are some examples:

\begin{verbatim}
class(lst[1])
class(lst[[1]])
\end{verbatim}

\begin{verbatim}
[1] "list"
[1] "data.frame"
\end{verbatim}

In the first instance, the output was a slice of the  list, in the second (double square brackets) it was the element itself, i.e. the data frame. You can therefore select multiple parts of a list using the concatenate function \texttt{c()}, alternatively, you can use indices to select elements from an element in the list. Here is an example of both:

\begin{verbatim}
lst[c(2,3)] # slice the second and third part of the list
lst[[c(2,12)]] # select the twelfth item of the second element (mat) in the list
lst[[2]][8,2] # select the item in the eigth row and second column in the second element of the list.
\end{verbatim}

\begin{verbatim}
[[1]]
        nums alph
row: 1  "1"  "a" 
row: 2  "2"  "b" 
row: 3  "3"  "c" 
row: 4  "4"  "d" 
row: 5  "5"  "e" 
row: 6  "6"  "f" 
row: 7  "7"  "g" 
row: 8  "8"  "h" 
row: 9  "9"  "i" 
row: 10 "10" "j" 

[[2]]
 [1] d e f j f f g i c c d d d g e d g f b c g d e i h c g c b f
Levels: b < c < d < e < f < g < h < i < j
[1] "b"
[1] "h"
\end{verbatim}

\subsubsection*{Using names in lists}
\label{sec:org9e53e86}

Lists, like data frames, can have named elements. In the examples below we first assign names to the elements of our list and then call items within these elements using these names.

\begin{verbatim}
names(lst) <-c("DataFrame", "Matrix", "Factor", "Integer")
head(lst$DataFrame)
lst$DataFrame[2,2]
\end{verbatim}

\begin{verbatim}
  nums ALPH fct1
1    1    G    d
2    2    I    e
3    3    J    f
4    4    D    j
5    5    E    f
6    6    F    f
[1] "I"
\end{verbatim}

\section*{Manipulating data using indices and other methods}
\label{sec:org212ef1a}

Thus far we've learned to address elements and items in objects using indices. Combining these indices with conditions provides us a powerful way to slice and dice data and to modify very specific parts of objects. 

We will use our dataframe for the following exercises. You should be able to repeat this on other objects described earlier.

\begin{itemize}
\item Select those rows in the dataframe where the value of the "nums" column is greater than 2.

\begin{verbatim}
with(datfrm,{
datfrm[nums>2,]
})
\end{verbatim}

\begin{verbatim}
   nums ALPH fct1
3     3    J    f
4     4    D    j
5     5    E    f
6     6    F    f
7     7    H    g
8     8    E    i
9     9    H    c
10   10    B    c
11   11    H    d
12   12    I    d
13   13    A    d
14   14    D    g
15   15    J    e
16   16    A    d
17   17    B    g
18   18    G    f
19   19    C    b
20   20    H    c
21   21    E    g
22   22    D    d
23   23    B    e
24   24    J    i
25   25    A    h
26   26    I    c
27   27    I    g
28   28    G    c
29   29    A    b
30   30    F    f
\end{verbatim}

You'd have noticed I'm using the \texttt{with()} function here to make the statements easier to type. This statement is the same as:

\begin{verbatim}
datfrm[datfrm$nums>2, ]
\end{verbatim}

\item Select rows where "nums" is greater or equal to 10 and "fct1" is a, b or c.

\begin{verbatim}
with(datfrm, {
  datfrm[nums>=10 & (fct1 == "a" | fct1 == "b" | fct1 == "c"), ]
})
\end{verbatim}

Or in a more efficient way using the \texttt{\%in\%} statement:

\begin{verbatim}
with(datfrm, {
  datfrm[nums >= 10 & fct1 %in% c("a", "b", "c"), ]
})
\end{verbatim}

\item Select only those rows where the data is complete.

There are a number of ways to do this, but before we can test this out, lets randomly insert "NAs" (not available) into a new column  of the dataframe. Borrowed from \href{https://stackoverflow.com/questions/27454265/r-randomly-insert-nas-into-dataframe-proportionaly}{here}.

\begin{verbatim}
datfrm$rlno <- runif(30, min = 0, max = 200)[sample(c(TRUE, NA), prob = c(0.65, 0.35), size = 30, replace = TRUE)]
head(datfrm$rlno)
\end{verbatim}

\begin{verbatim}
[1] 193.13802  92.13322 137.70294 139.18637  16.56442        NA
\end{verbatim}

Now lets clean up the dataframe.

\begin{verbatim}
na.omit(datfrm)
datfrm[complete.cases(datfrm), ]
datfrm[!is.na(datfrm$rlno),]
\end{verbatim}

\item Replace selected values

This is a simple extension of the indexing operation by assigning the replacement to it. For example, if you wanted to replace the NAs with zero you'd do:

\begin{verbatim}
datfrm$rlno[is.na(datfrm$rlno)] <- 0
head(datfrm)
\end{verbatim}

\begin{verbatim}
  nums ALPH fct1      rlno
1    1    G    d 193.13802
2    2    I    e  92.13322
3    3    J    f 137.70294
4    4    D    j 139.18637
5    5    E    f  16.56442
6    6    F    f   0.00000
\end{verbatim}
\end{itemize}


---
\end{document}
