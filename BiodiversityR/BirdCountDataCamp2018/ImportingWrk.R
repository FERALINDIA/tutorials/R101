library(auk)
library(magrittr)
vignette("auk")

    
    library(auk)
    # path to the ebird data file, here a sample included in the package
    input_file <- system.file("extdata/ebd-sample.txt", package = "auk")
    # output text file
    output_file <- "ebd_filtered_grja.txt"
    ebird_data <- input_file %>% 
    # 1. reference file
    auk_ebd() %>% 
    # 2. define filters
    auk_species(species = "Gray Jay") %>% 
    auk_country(country = "Canada") %>% 
    # 3. run filtering
    auk_filter(file = output_file) %>% 
    # 4. read text file into r data frame
        read_ebd()

input_file <- system.file("extdata/ebd-sample.txt", package = "auk")
output_file <- "ebd_filtered_grja.txt"
ebd <- auk_ebd(input_file)
ebd_filters <- auk_species(ebd, species = "Gray Jay")
ebd_filters <- auk_country(ebd_filters, country = "Canada")
ebd_filtered <- auk_filter(ebd_filters, file = output_file)
ebd_df <- read_ebd(ebd_filtered)

